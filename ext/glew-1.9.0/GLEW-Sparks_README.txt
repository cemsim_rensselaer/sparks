
The C/C++ Runtime libraries linked by the distributed GLEW binaries and the default build settings of GLEW are the non-DLL versions, i.e., /MT and /MTd options in VS.

This directory holds the 1.9.0 release build with the /MD and /MDd options to be compatible with the sparks library.