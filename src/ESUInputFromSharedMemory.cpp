#include "ESUInputFromSharedMemory.hpp"

const char* spark::g_sharedMemorySectionName = "SPARK_SharedMemory";
const char* spark::g_sharedMemoryObjectName = "CurrentESUSettings";
