-- Load Common Textures

textureManager:loadCheckerTexture( "checker", -1 );
textureManager:loadTestTexture( "test", -1 );
textureManager:loadTextureFromImageFile( "cat", "sample.png" );
-- textureManager:loadTextureFromImageFile( "skinColor", "skin_tile.png" );
textureManager:loadTextureFromImageFile( "sparkColor", "sparkCircularGradient.png" );
textureManager:loadTextureFromImageFile( "hook_cautery", "hook_cautery_noise.png" )
textureManager:loadTextureFromImageFile( "noise_normalMap", "noise_normalMap.jpg" )
textureManager:loadTextureFromImageFile( "cloth", "cloth.jpg" )

-- textureManager:loadTextureFromImageFile( "tissueDiffuse", "Leather0013_1_L.jpg" )
-- textureManager:loadTextureFromImageFile( "tissueNormal", "liver1024_bump.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueBump", "liver1024_bump.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueSpecular", "liverTexture_specularStrength.png" )
-- textureManager:loadTextureFromImageFile( "tissueAmbient", "liverTexture_ambientOcclusion.png" )

-- textureManager:loadTextureFromImageFile( "tissueDiffuse", "liverTexture_diffuse.png" )
-- textureManager:loadTextureFromImageFile( "tissueNormal", "liverTexture_normal.png" )
-- textureManager:loadTextureFromImageFile( "tissueBump", "liverTexture_bump.png" )
-- textureManager:loadTextureFromImageFile( "tissueSpecular", "liverTexture_specularStrength.png" )
-- textureManager:loadTextureFromImageFile( "tissueAmbient", "liverTexture_ambientOcclusion.png" )

-- textureManager:loadTextureFromImageFile( "tissueDiffuse", "liver.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueNormal", "liver1024_bump.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueBump", "liverTexture_bump.png" )
-- textureManager:loadTextureFromImageFile( "tissueSpecular", "liverTexture_specularStrength.png" )
-- textureManager:loadTextureFromImageFile( "tissueAmbient", "liverTexture_ambientOcclusion.png" )

-- textureManager:loadTextureFromImageFile( "tissueDiffuse", "liver.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueNormal", "skin_normal.jpg" )
-- textureManager:loadTextureFromImageFile( "tissueBump", "liver1024_bump.bmp" )
-- textureManager:loadTextureFromImageFile( "tissueSpecular", "liverTexture_specularStrength.png" )
-- textureManager:loadTextureFromImageFile( "tissueAmbient", "liverTexture_ambientOcclusion.png" )

--original textureManager:loadTextureFromImageFile( "tissueDiffuse", "liver1024.bmp" )  baichun 2014-03-18
--textureManager:loadTextureFromImageFile( "tissueDiffuse", "steak.bmp")-- "liver_5.bmp" )
--textureManager:loadTextureFromImageFile( "tissueBump", "steak_bump.bmp")--"liver1024_bump.bmp" )
--textureManager:loadTextureFromImageFile( "tissueNormal", "steak_normal.bmp")--"ground-normal.jpg" )
--textureManager:loadTextureFromImageFile( "tissueCharNormal", "steak_normal.bmp")--"ground-normal.jpg" )
-- textureManager:loadTextureFromImageFile( "tissueNormal", "liver1024_bump.jpg" )

-- Ganesh modifications 07/09/14
  textureManager:loadTextureFromImageFile( "tissueDiffuse", "Test1.jpg")-- "liver_5.bmp" )
  textureManager:loadTextureFromImageFile( "tissueBump", "Test1_bump.jpg")--"liver1024_bump.bmp" )
  textureManager:loadTextureFromImageFile( "tissueNormal", "Test1_normal.jpeg")--"ground-normal.jpg" )
  textureManager:loadTextureFromImageFile( "tissueCharNormal", "Test1.jpg")--"ground-normal.jpg" )

-- textureManager:loadTextureFromImageFile( "tissueSpecular", "liverTexture_specularStrength.png" )
-- textureManager:loadTextureFromImageFile( "tissueAmbient", "liverTexture_ambientOcclusion.png" )


textureManager:loadTextureFromImageFile( "bgCloth", "seamlesstexture18_1200.jpg" )

--Baichun 2014-03-19
textureManager:loadTextureFromImageFile( "CutWave", "Cut.jpg" )
textureManager:loadTextureFromImageFile( "BlendWave", "Blend.jpg" )
textureManager:loadTextureFromImageFile( "CoagWave", "Coag.jpg" )


textureManager:logTextures();
