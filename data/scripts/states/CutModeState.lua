----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Sim = require "Sim"
local ESUModel = require "ESUModel"
----------------------------------------

--[[

--]]

CutModeState = {}

function CutModeState:new()
    print( "CutModeState:new" )
    newObj = 
    { 
        buttons = {}, 
        hasVibrated = false,
        startTime = -1, 
        activationTime = 0,
        currTime = 0, 
        contactArea = 0,  -- area of contact (m^2) between tissue and electrode
		radiusOfContact = 0.0002,
		tool = 0,
        tissueDistance = 0,  -- distance to tissue in mm
        theNextState = "",
		temperature = 36.8,     --baichun 2014-03-21
		show_cuttask1_text = true,
    }
    self.__index = self
    return setmetatable(newObj, self)
end


function CutModeState:load()
    print( "CutModeState:load" )

    local isShadowOn = true

    Render:createDefaultRenderPasses( isShadowOn )

    Sim.load( self )

    ESUModel.theESUModel:createSpark()

	self.targetWattage = 20  --Ganesh changed from 30
	self.targetMode = ESUINPUT_CUT
	Sim.createInstructionText( self, string.format( 
	[[
	Change the MODE setting on the ESU 
	touchscreen to "CUT" mode (yellow),
  
	And set the power to %d watts.
 
	(Waiting...)
	]], self.targetWattage ) )
		self.step = "Settings"
end

function CutModeState:activate()
    print( "CutModeState:activate" )
    self.startTime = -1
    self.theNextState = ""

    Sim.activate( self )
end


function CutModeState:update( dt )
    -- Convey updates from the UI to the current ESU settings
    ESUModel.theESUModel:updateInput( theESUInput )
	--self.targetWattage = ESUModel.theESUModel.cutWattage*0.1;
	if(self.show_cuttask1_text) then
		if self.step == "Settings" 
			and ESUModel.theESUModel.mode == self.targetMode 
			and ESUModel.theESUModel.cutWattage*0.1 == self.targetWattage then
				self.instructionText:setText(string.format(
	[[
	The ESU is now in CUT mode
	and at %d watts power setting.

	Task 1:
	(1) Hold the electrosurgery pencil
	tool near but not in direct contact
	with tissue and activate in a 
	straight line.

	(2) Now repeat the cutting task while
	in direct contact with the tissue.

	NOTICE:

	(1) No visible coagulum when not in
	contact with the tissue.
	(2) Visible coagulum when in contact 
	with the tissue.

	Press "spacebar" when finished.
	]],ESUModel.theESUModel.cutWattage*0.1))
	--print (ESUModel.theESUModel.cutWattage*0.1)

		end
	else
		if self.step == "Settings" 
			and ESUModel.theESUModel.mode == self.targetMode 
			and ESUModel.theESUModel.cutWattage*0.1 == self.targetWattage then
				self.instructionText:setText(string.format(
	[[
	The ESU is now in CUT mode
	and at %d watts power setting.

	Task 1:
	(1) Hold the electrosurgery pencil
	tool near but not in direct contact
	with tissue and activate in a 
	straight line.

	(2) Now repeat the cutting task while
	in direct contact with the tissue.

	NOTICE:

	(1) No visible coagulum and larger 
	incision compared to the power setting
	of 20 when not in contact with the
	tissue.
	(2) Formation of black coagulum and no
	incision when in contact with the
	tissue.

	Press "Enter" when finished.
	]],ESUModel.theESUModel.cutWattage*0.1))
	--print (ESUModel.theESUModel.cutWattage*0.1)
		else
			self.targetMode = ESUINPUT_CUT
			self.instructionText:setText(string.format( 
			[[
			 Set the CUT power to %d watts.
 
			(Waiting...)
			]], self.targetWattage ) )
				self.step = "Settings"
		end
	end

	if input:isKeyDown( KEY_KP_SPACE ) then
	    self.show_cuttask1_text = false
		self.targetWattage = 40
    end

	if input:isKeyDown( KEY_KP_ENTER ) then
       -- self.theNextState = "Menu"
	   --self.theNextState="Module1B"
	     self.theNextState="CoagMode"
    end

    Sim.update( self, dt )
end

function CutModeState:deactivate()
    print( "CutModeState:deactivate" )
end

function CutModeState:nextState( currTime )
    -- if input:isKeyDown( KEY_ESC ) then
    --     self.theNextState = "Loading"
    -- end
 
    if input:isButtonPressed( "stylus", 2 ) then
        --self.theNextState = "Menu"  --baichun
    end
    theNextState = self.theNextState
end

theState = CutModeState:new()
theNextState = ""

