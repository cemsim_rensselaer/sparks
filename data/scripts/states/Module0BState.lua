----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Fonts = require "Fonts"
Fonts:init()
----------------------------------------
-- Module1 state will load the learning objective and walk the subject through the didactic material
Module0BState = {}
 
function Module0BState:new()
	print( "Module0BState:new" )
	newObj = { 
		angle = 45, 
		hasRunOnce = false, 
		startTime = -1,
		buttons = {}, 
		enableCoag=1,
		theNextState = ""
	}
	self.__index = self
	return setmetatable(newObj, self)
end

function Module0BState:load()
	print( "Module0BState:load" )

	spark:createPostProcessingRenderPass( 0.0, 
	      "MSAAFinalRenderPass",
	      "MainRenderTargetTexture", spark:getFrameBufferRenderTarget(), 
	      "texturedOverlayShader" )

	mainRenderTarget = spark:createTextureRenderTarget( "MainRenderTargetTexture" )
	spark:setMainRenderTarget( mainRenderTarget )
	mainRenderTarget:setClearColor( vec4( 0.2,0.2,0.2,0.5 ) )

	HUDRenderPass = spark:createOverlayRenderPass( 0.25, "HUDPass", mainRenderTarget )
	HUDRenderPass:setDepthTest( false )
	HUDRenderPass:setDepthWrite( false )
	HUDRenderPass:useInterpolatedBlending()

	local fontMgr = spark:getFontManager()

	explanationMat = spark:createMaterial( "TextShader" )
	explanationMat:addTexture( "s_color", fontMgr:getFontAtlasTextureName() )
	explanationMat:setVec4( "u_color", vec4( 0.8, 0.8, 0.8,1 ) )
	self.explanationText = spark:createText( Fonts.defaultFontName, 
		                                     Fonts.defaultFontSmallButtonSize,--defaultFontTextSize, 
		                                     explanationMat, 
		                            		 "HUDPass", "..." )
	self.explanationText:translate( vec3( 0.1, 0.65, 0 ) ) 

	if(self.enableCoag == 1) then
	self.explanationText:setText([[
B. Blend Mode

Blend mode is NOT a combination of cut and coag.  Blend mode is an interrupted 
form of cut.  It is a continuous wave form but the ESU only activates energy for part 
of the time.  The voltage is higher than on pure cut.  Blend mode is used when
hemostasis is needed while making the incision. It will also help the surgeon make an 
incision through tissue with higher impedance.  
]]
	 )

	 end


	 if(self.enableCoag == 2) then
	 print("Should Display Coag Mode")
	self.explanationText:setText([[
C. Coag Mode

 
 Coag mode is an interrupted, high voltage wave form. Compared to cut there is an
 uneven and unpredictable coagulation pattern. The more superficial layers of the 
 tissue become rapidly coagulated, increasing impedance, thereby preventing further
 transmission of the energy to the deeper layers of the tissue. There is not reliable
 sealing of vessels When this is used without direct tissue contact, with high voltage,
 very high temperatures are reached, resulting in fulguration, which is useful for 
 superficial coagulation of small capillaries and raw surfaces, such as the gallbladder 
 fossa of the liver.
 
]]
	 )

	 end

	-------------------------------------------------
	-- Buttons
	local xpos = 0.1
	local ypos = 0.4
	local lineHeight = 0.15
-- 	self.buttons["Introduction"] = Button:newLargeButton( xpos, ypos, "1", "Introduction" )
-- 	self.buttons["Introduction"].onClick = function () self.theNextState = "Introduction" end
-- 	self.buttons["Introduction"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[A brief overview of the VEST hands-on 
-- electosurgery simulator.]])
-- 	end

	ypos = ypos - lineHeight

	--if(self.enableCoag == 1) then
	self.buttons["Start the Module"] = Button:newLargeButton( xpos, ypos, KEY_KP_2, "Click to Continue" )
	--end
	--if(self.enableCoag == 2) then
	--self.buttons["Start the Module"] = Button:newLargeButton( xpos, ypos, KEY_KP_3, " " )
	--end


	self.buttons["Start the Module"].onClick = function () 
		self.theNextState = "FreeModeA" --"CutMode" --"FreeModeB"--"CutMode" --"FreeMode" 
		print("Switching to FreeModeB state")
end
	self.buttons["Start the Module"].onMouseOver = function ()
		self.explanationText:setText( 
[[ ]])
	end

	self.buttons["Start the Module"].onMouseOut = function ()


	   if(self.enableCoag == 1) then
	self.explanationText:setText([[
B. Blend Mode

Blend mode is NOT a combination of cut and coag.  Blend mode is an interrupted 
form of cut.  It is a continuous wave form but the ESU only activates energy for part 
of the time.  The voltage is higher than on pure cut.  Blend mode is used when
hemostasis is needed while making the incision. It will also help the surgeon make an 
incision through tissue with higher impedance.  
]]
	 )

	 end


	 if(self.enableCoag == 2) then
	 print("Should Display Coag Mode")
	self.explanationText:setText([[
	
C. Coag Mode
 
 Coag mode is an interrupted, high voltage wave form. Compared to cut there is an
 uneven and unpredictable coagulation pattern. The more superficial layers of the 
 tissue become rapidly coagulated, increasing impedance, thereby preventing further
 transmission of the energy to the deeper layers of the tissue. There is not reliable
 sealing of vessels When this is used without direct tissue contact, with high voltage,
 very high temperatures are reached, resulting in fulguration, which is useful for 
 superficial coagulation of small capillaries and raw surfaces, such as the gallbladder 
 fossa of the liver.
 
]]
	 )

	 end


	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Contact Area"] = Button:newLargeButton( xpos, ypos, KEY_KP_3, "[3] Contact Area" )
-- 	self.buttons["Contact Area"].onClick = function ()  self.theNextState = "" end
-- 	self.buttons["Contact Area"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[The area of contact between the electrode 
-- and the tissue has a surprisingly large impact on the
-- heating effect.]])
-- 	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Freestyle"] = Button:newLargeButton( xpos, ypos, KEY_KP_4, "[4] Simulation" )
-- 	self.buttons["Freestyle"].onClick = function ()  self.theNextState = "Simulation" end
-- 	self.buttons["Freestyle"].onMouseOver = function () 
-- 		self.explanationText:setText( 
-- [[Experiment freely on simulated tisse.]])
-- 	end

	self.cursorScale = 0.025
	local cursorMat = spark:createMaterial( "constantColorShader" )
	cursorMat:setVec4( "u_color", vec4( 1, 0.5, 0.5, 0.5) );
	self.cursor = spark:loadMesh( "sphere.obj", cursorMat, "HUDPass" )

end

function Module0BState:activate()
	print( "Module0BState:activate" )
	self.startTime = -1
	self.theNextState = ""
	print(self.enableCoag)
	Module0BState:load()
end

function Module0BState:update( dt )
	stylusPos = input:getDefaultDevicePosition()
	stylusScreenPos = input:getDefaultDeviceScreenPosition()
	stylusMat = input:getDefaultDeviceTransform( )
	buttonState = input:isDefaultDeviceButtonPressed( 0 )

	self.cursor:setTransform( mat4() )
	self.cursor:translate( vec3(stylusScreenPos.x, stylusScreenPos.y, 0.1) )
	self.cursor:scale( self.cursorScale )
	self.cursor:scale( vec3(1080/1920, 1, 1) )
	for id, button in pairs( self.buttons ) do
		button:update( stylusScreenPos.x, stylusScreenPos.y, buttonState )
	end
	if input:isKeyDown( KEY_KP_ENTER ) then
	    self.theNextState = "Module1"
    end


	if(self.enableCoag == 1) then
		textureManager:loadTextureFromImageFile( "cemsimLogo", "Blend.jpg" )
	end

	if(self.enableCoag == 2) then
		textureManager:loadTextureFromImageFile( "cemsimLogo", "Coag.jpg" )
	end
	self.logoMaterial = spark:createMaterial( "texturedOverlayShader" )
	self.logoMaterial:addTexture( "s_color", "cemsimLogo" )
	local logoX = 512  -- actual size in pixels
	local logoY = 512
	local displayAspectRatio = 1080/1920 -- zSpace display
	local aspect = (logoY / logoX) / displayAspectRatio -- .75 is aspect ratio of display
	self.size = 1
	local size = self.size
	local sizeScale = 0.15
	self.logo = spark:createQuad( vec2( 0.82, 0.92 ), -- position
		                          vec2( sizeScale*size, -size*aspect*sizeScale ),  --size
		                          self.logoMaterial, "HUDPass" )
end

function Module0BState:deactivate()
	print( "Module0BState:deactivate" )
	if(self.enableCoag == 1) then
		self.enableCoag=2
	--end
	else 
		self.enableCoag=1
	end
	--print(self.enableCoag)
	self.theNextState = ""
end

function Module0BState:nextState( currTime )
	theNextState = self.theNextState
end

theState = Module0BState:new()
theNextState = ""

