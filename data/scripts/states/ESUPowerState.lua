----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Sim = require "Sim"
local ESUModel = require "ESUModel"
----------------------------------------

--[[

--]]

ESUPowerState = {}

function ESUPowerState:new()
    print( "ESUPowerState:new" )
    newObj = 
    { 
        buttons = {}, 
        hasVibrated = false,
        startTime = -1, 
        activationTime = 0,
        currTime = 0, 
        contactArea = 0,  -- area of contact (m^2) between tissue and electrode
        tissueDistance = 0,  -- distance to tissue in mm
        theNextState = "",
		temperature = 36.8,     --baichun 2014-03-21

    }
    self.__index = self
    return setmetatable(newObj, self)
end


function ESUPowerState:load()
    print( "ESUPowerState:load" )

    local isShadowOn = true

    Render:createDefaultRenderPasses( isShadowOn )

    Sim.load( self )

    ESUModel.theESUModel:createSpark()

    self.targetWattage = 20
    self.targetMode = ESUINPUT_CUT
    Sim.createInstructionText( self, string.format( 
[[
Change the MODE setting on the ESU 
touchscreen to "CUT" mode (yellow),
 
And set the power to %d watts.
 
(Waiting...)
]], self.targetWattage ) )
    self.step = "Settings"

end

function ESUPowerState:activate()
    print( "ESUPowerState:activate" )
    self.startTime = -1
	self.theNextState = ""       --Baichun 2014-03-21

    Sim.activate( self )
end

-- Added by Ganesh  03/22/14
function ESUPowerState:update( dt )
    -- Convey updates from the UI to the current ESU settings
    ESUModel.theESUModel:updateInput( theESUInput )

    if self.step == "Settings" 
        and ESUModel.theESUModel.mode == self.targetMode 
        and ESUModel.theESUModel.cutWattage*0.1 == self.targetWattage then
            self.instructionText:setText( string.format(
[[
The ESU is now in CUT mode
and at %d watts power setting.

Task1: Hold the pencil tool near
but not in direct contact with
tissue and activate in a 
straight line.

Task2: Now repeat task1 with contact.
Now change the power setting to 40.
Repeat task1 and task2

Press "Enter" when finished.
]], ESUModel.theESUModel.cutWattage*0.1) )  --self.targetWattage
self.targetWattage =ESUModel.theESUModel.cutWattage*0.1 
    end

    Sim.update( self, dt )
end

function ESUPowerState:deactivate()
    print( "ESUPowerState:deactivate" )
end

function ESUPowerState:nextState( currTime )
    if input:isKeyDown( KEY_KP_ENTER ) then
        self.theNextState = "Menu"
    end

    if input:isButtonPressed( "stylus", 2 ) then
        --self.theNextState = "Menu"   baichun
    end
    theNextState = self.theNextState
end

theState = ESUPowerState:new()
theNextState = ""

