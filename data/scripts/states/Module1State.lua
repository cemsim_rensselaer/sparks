----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Fonts = require "Fonts"
Fonts:init()
----------------------------------------
-- Module1 state will load the learning objective and walk the subject through the didactic material
Module1State = {}
 
function Module1State:new()
	print( "Module1State:new" )
	newObj = { 
		angle = 45, 
		hasRunOnce = false, 
		startTime = -1,
		buttons = {}, 
		theNextState = ""
	}
	self.__index = self
	return setmetatable(newObj, self)
end

function Module1State:load()
	print( "Module1State:load" )

	spark:createPostProcessingRenderPass( 0.0, 
	      "MSAAFinalRenderPass",
	      "MainRenderTargetTexture", spark:getFrameBufferRenderTarget(), 
	      "texturedOverlayShader" )

	mainRenderTarget = spark:createTextureRenderTarget( "MainRenderTargetTexture" )
	spark:setMainRenderTarget( mainRenderTarget )
	mainRenderTarget:setClearColor( vec4( 0.2,0.2,0.2,0.5 ) )

	HUDRenderPass = spark:createOverlayRenderPass( 0.25, "HUDPass", mainRenderTarget )
	HUDRenderPass:setDepthTest( false )
	HUDRenderPass:setDepthWrite( false )
	HUDRenderPass:useInterpolatedBlending()

	local fontMgr1 = spark:getFontManager()

	explanationMat1 = spark:createMaterial( "TextShader" )
	explanationMat1:addTexture( "s_color", fontMgr1:getFontAtlasTextureName() )
	explanationMat1:setVec4( "u_color", vec4( 0.8, 0.8, 0.8,1 ) )
	self.explanationText1 = spark:createText( Fonts.defaultFontName, 
		                                     Fonts.defaultFontSmallButtonSize,--defaultFontTextSize, 
		                                     explanationMat1, 
		                            		 "HUDPass", "..." )
	self.explanationText1:translate( vec3( 0.1, 0.9, 0 ) ) 
	self.explanationText1:setText( 
	[[The Virtual Electrosurgery Skills Trainer (VEST)]]
	 )



	local fontMgr = spark:getFontManager()

	explanationMat = spark:createMaterial( "TextShader" )
	explanationMat:addTexture( "s_color", fontMgr:getFontAtlasTextureName() )
	explanationMat:setVec4( "u_color", vec4( 0.8, 0.8, 0.8,1 ) )
	self.explanationText = spark:createText( Fonts.defaultFontName, 
		                                     Fonts.defaultFontSmallButtonSize,--defaultFontTextSize, 
		                                     explanationMat, 
		                            		 "HUDPass", "..." )
	self.explanationText:translate( vec3( 0.1, 0.65, 0 ) ) 
	self.explanationText:setText([[ 
	Learning Objectives:

	1. Difference in waveforms between cut, blend and coag settings
	2. Different power settings and its effect on cut, coag and blend]]
	 )

	-------------------------------------------------
	-- Buttons
	local xpos = 0.1
	local ypos = 0.9
	local lineHeight = 0.15
-- 	self.buttons["Introduction"] = Button:newLargeButton( xpos, ypos, "1", "Introduction" )
-- 	self.buttons["Introduction"].onClick = function () self.theNextState = "Introduction" end
-- 	self.buttons["Introduction"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[A brief overview of the VEST hands-on 
-- electosurgery simulator.]])
-- 	end

	ypos = ypos - lineHeight
	self.buttons["Start the Module"] = Button:newLargeButton( xpos, ypos, KEY_KP_1, "Module 1" )
	self.buttons["Start the Module"].onClick = function () 
		--self.theNextState = "Module1A" 
		--self.theNextState ="CutMode"
		self.theNextState ="Module0A"  --"FreeMode"
		print("Switching to Module 0 A state")
end
	self.buttons["Start the Module"].onMouseOver = function ()
		self.explanationText:setText( 
[[Click the button to start the Module.]])
	end

	self.buttons["Start the Module"].onMouseOut = function ()
		self.explanationText:setText( 
[[Learning Objectives:

	1. Difference in waveforms between cut and coag settings
	2. Different power settings and its effect on both cut and coag.]])
	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Contact Area"] = Button:newLargeButton( xpos, ypos, KEY_KP_3, "[3] Contact Area" )
-- 	self.buttons["Contact Area"].onClick = function ()  self.theNextState = "" end
-- 	self.buttons["Contact Area"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[The area of contact between the electrode 
-- and the tissue has a surprisingly large impact on the
-- heating effect.]])
-- 	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Freestyle"] = Button:newLargeButton( xpos, ypos, KEY_KP_4, "[4] Simulation" )
-- 	self.buttons["Freestyle"].onClick = function ()  self.theNextState = "Simulation" end
-- 	self.buttons["Freestyle"].onMouseOver = function () 
-- 		self.explanationText:setText( 
-- [[Experiment freely on simulated tisse.]])
-- 	end

	self.cursorScale = 0.025
	local cursorMat = spark:createMaterial( "constantColorShader" )
	cursorMat:setVec4( "u_color", vec4( 1, 0.5, 0.5, 0.5) );
	self.cursor = spark:loadMesh( "sphere.obj", cursorMat, "HUDPass" )

end

function Module1State:activate()
	print( "Module1State:activate" )
	self.startTime = -1
	self.theNextState = ""
end

function Module1State:update( dt )
	stylusPos = input:getDefaultDevicePosition()
	stylusScreenPos = input:getDefaultDeviceScreenPosition()
	stylusMat = input:getDefaultDeviceTransform( )
	buttonState = input:isDefaultDeviceButtonPressed( 0 )

	self.cursor:setTransform( mat4() )
	self.cursor:translate( vec3(stylusScreenPos.x, stylusScreenPos.y, 0.1) )
	self.cursor:scale( self.cursorScale )
	self.cursor:scale( vec3(1080/1920, 1, 1) )
	for id, button in pairs( self.buttons ) do
		button:update( stylusScreenPos.x, stylusScreenPos.y, buttonState )
	end
end

function Module1State:deactivate()
	print( "Module1State:deactivate" )
	self.theNextState = ""
end

function Module1State:nextState( currTime )
	theNextState = self.theNextState
end

theState = Module1State:new()
theNextState = ""

