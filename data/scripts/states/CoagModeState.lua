----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Sim = require "Sim"
local ESUModel = require "ESUModel"
----------------------------------------

--[[

--]]

CoagModeState = {}

function CoagModeState:new()
    print( "CoagModeState:new" )
    newObj = 
    { 
        buttons = {}, 
        hasVibrated = false,
        startTime = -1, 
        activationTime = 0,
        currTime = 0, 
        contactArea = 0,  -- area of contact (m^2) between tissue and electrode
		radiusOfContact = 0.002,
		tool = 0,
        tissueDistance = 0,  -- distance to tissue in mm
        theNextState = "",
		temperature = 36.8,     --baichun 2014-03-21
    }
    self.__index = self
    return setmetatable(newObj, self)
end


function CoagModeState:load()
    print( "CoagModeState:load" )

    local isShadowOn = true

    Render:createDefaultRenderPasses( isShadowOn )

    Sim.load( self )

    ESUModel.theESUModel:createSpark()

    self.targetWattage = 60
    Sim.createInstructionText( self, string.format( 
[[
Change the MODE setting on the ESU 
touchscreen to "COAG" mode (blue),
 
And set the power to %d watts.
 
(Waiting...)
]], self.targetWattage ) )
    self.step = "Settings"

    -- tissueMat:setVec2( "u_targetCircleCenter", vec2( 0.4, 0.4 ) )
    -- tissueMat:setFloat( "u_targetCircleOuterRadius", 0.025 )
    -- tissueMat:setFloat( "u_targetCircleInnerRadius", 0.024 )

end

function CoagModeState:activate()
    print( "CoagModeState:activate" )
    self.startTime = -1
    self.theNextState = ""

    Sim.activate( self )
end


function CoagModeState:update( dt )
    -- Convey updates from the UI to the current ESU settings
    ESUModel.theESUModel:updateInput( theESUInput )

    if self.step == "Settings" 
        and ESUModel.theESUModel.mode == ESUINPUT_COAG 
        and ESUModel.theESUModel.coagWattage*0.1 == self.targetWattage then
            self.instructionText:setText( string.format(
[[
The ESU is now in COAG mode.
and at %d watts power setting.

Task 1: Hold the electrosurgical pencil in 
contact with the tissue and activate it in
a painting like manner across the tissue.

Task 2: Now repeat Task 1 but hold it near
and do not make contact with the tissue.

NOTICE:
(1) When in contact, formation of black
coagulum which flakes off when contact is 
released.
(2) When not in contact, formation of 
widespread black coagulum.
 
Press "Enter" when finished.
]], ESUModel.theESUModel.coagWattage*0.1) )
    end

    Sim.update( self, dt )
end

function CoagModeState:deactivate()
    print( "CoagModeState:deactivate" )
end

function CoagModeState:nextState( currTime )
    if input:isKeyDown( KEY_KP_ENTER ) then
        self.theNextState = "Module1"
    end

    if input:isButtonPressed( "stylus", 2 ) then
        --self.theNextState = "Menu"  --baichun
    end
    theNextState = self.theNextState
end

theState = CoagModeState:new()
theNextState = ""

