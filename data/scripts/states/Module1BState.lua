----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Fonts = require "Fonts"
Fonts:init()
----------------------------------------
-- Module1 state will load the learning objective and walk the subject through the didactic material
Module1BState = {}
 
function Module1BState:new()
	print( "Module1BState:new" )
	newObj = { 
		angle = 45, 
		hasRunOnce = false, 
		startTime = -1,
		buttons = {}, 
		theNextState = ""
	}
	self.__index = self
	return setmetatable(newObj, self)
end

function Module1BState:load()
	print( "Module1BState:load" )

	spark:createPostProcessingRenderPass( 0.0, 
	      "MSAAFinalRenderPass",
	      "MainRenderTargetTexture", spark:getFrameBufferRenderTarget(), 
	      "texturedOverlayShader" )

	mainRenderTarget = spark:createTextureRenderTarget( "MainRenderTargetTexture" )
	spark:setMainRenderTarget( mainRenderTarget )
	mainRenderTarget:setClearColor( vec4( 0.2,0.2,0.2,0.5 ) )

	HUDRenderPass = spark:createOverlayRenderPass( 0.25, "HUDPass", mainRenderTarget )
	HUDRenderPass:setDepthTest( false )
	HUDRenderPass:setDepthWrite( false )
	HUDRenderPass:useInterpolatedBlending()

	local fontMgr = spark:getFontManager()

	explanationMat = spark:createMaterial( "TextShader" )
	explanationMat:addTexture( "s_color", fontMgr:getFontAtlasTextureName() )
	explanationMat:setVec4( "u_color", vec4( 0.8, 0.8, 0.8,1 ) )
	self.explanationText = spark:createText( Fonts.defaultFontName, 
		                                     Fonts.defaultFontSmallButtonSize,--defaultFontTextSize, 
		                                     explanationMat, 
		                            		 "HUDPass", "..." )
	self.explanationText:translate( vec3( 0.1, 0.65, 0 ) ) 
	self.explanationText:setText([[ 
Coag mode is an interrupted, high voltage wave form.
Compared to cut there is an uneven and unpredictable coagulation pattern. 
The more superficial layers of the tissue become rapidly coagulated, increasing
impedance, thereby preventing further transmission of the energy to the deeper 
layers of the tissue. There is not reliable sealing of vessels When this is used
without direct tissue contact, with high voltage, very high temperatures are reached,
resulting in fulguration, which is useful for superficial coagulation of small capillaries
and raw surfaces, such as the gallbladder fossa of the liver.
]]
	 )

	-------------------------------------------------
	-- Buttons
	local xpos = 0.1
	local ypos = 0.9
	local lineHeight = 0.15
-- 	self.buttons["Introduction"] = Button:newLargeButton( xpos, ypos, "1", "Introduction" )
-- 	self.buttons["Introduction"].onClick = function () self.theNextState = "Introduction" end
-- 	self.buttons["Introduction"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[A brief overview of the VEST hands-on 
-- electosurgery simulator.]])
-- 	end

	ypos = ypos - lineHeight
	self.buttons["Coag"] = Button:newLargeButton( xpos, ypos, KEY_KP_5, "B. CoagMode" )
	self.buttons["Coag"].onClick = function () 
		self.theNextState = "CoagMode" 
		print("Switching to ESUcoagMode state")
end
	self.buttons["Coag"].onMouseOver = function ()
		self.explanationText:setText( 
[[Click the button to Continue]])
	end

	self.buttons["Coag"].onMouseOut = function ()
		self.explanationText:setText( 
[[Coag mode is an interrupted, high voltage wave form.
Compared to cut there is an uneven and unpredictable coagulation pattern. 
The more superficial layers of the tissue become rapidly coagulated, increasing
impedance, thereby preventing further transmission of the energy to the deeper 
layers of the tissue. There is not reliable sealing of vessels When this is used
without direct tissue contact, with high voltage, very high temperatures are reached,
resulting in fulguration, which is useful for superficial coagulation of small capillaries
and raw surfaces, such as the gallbladder fossa of the liver.
]])
	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Contact Area"] = Button:newLargeButton( xpos, ypos, KEY_KP_3, "[3] Contact Area" )
-- 	self.buttons["Contact Area"].onClick = function ()  self.theNextState = "" end
-- 	self.buttons["Contact Area"].onMouseOver = function () 
-- 		self.explanationText:setText(
-- [[The area of contact between the electrode 
-- and the tissue has a surprisingly large impact on the
-- heating effect.]])
-- 	end

-- 	ypos = ypos - lineHeight
-- 	self.buttons["Freestyle"] = Button:newLargeButton( xpos, ypos, KEY_KP_4, "[4] Simulation" )
-- 	self.buttons["Freestyle"].onClick = function ()  self.theNextState = "Simulation" end
-- 	self.buttons["Freestyle"].onMouseOver = function () 
-- 		self.explanationText:setText( 
-- [[Experiment freely on simulated tisse.]])
-- 	end

	self.cursorScale = 0.025
	local cursorMat = spark:createMaterial( "constantColorShader" )
	cursorMat:setVec4( "u_color", vec4( 1, 0.5, 0.5, 0.5) );
	self.cursor = spark:loadMesh( "sphere.obj", cursorMat, "HUDPass" )

end

function Module1BState:activate()
	print( "Module1BState:activate" )
	self.startTime = -1
	self.theNextState = ""
end

function Module1BState:update( dt )
	stylusPos = input:getDefaultDevicePosition()
	stylusScreenPos = input:getDefaultDeviceScreenPosition()
	stylusMat = input:getDefaultDeviceTransform( )
	buttonState = input:isDefaultDeviceButtonPressed( 0 )

	self.cursor:setTransform( mat4() )
	self.cursor:translate( vec3(stylusScreenPos.x, stylusScreenPos.y, 0.1) )
	self.cursor:scale( self.cursorScale )
	self.cursor:scale( vec3(1080/1920, 1, 1) )
	for id, button in pairs( self.buttons ) do
		button:update( stylusScreenPos.x, stylusScreenPos.y, buttonState )
	end
	--if input:isKeyDown( KEY_KP_ENTER ) then
	    --self.theNextState = "CoagMode"
    --end

	textureManager:loadTextureFromImageFile( "cemsimLogo", "Coag.jpg" )
	self.logoMaterial = spark:createMaterial( "texturedOverlayShader" )
	self.logoMaterial:addTexture( "s_color", "cemsimLogo" )
	local logoX = 512  -- actual size in pixels
	local logoY = 512
	local displayAspectRatio = 1080/1920 -- zSpace display
	local aspect = (logoY / logoX) / displayAspectRatio -- .75 is aspect ratio of display
	self.size = 1
	local size = self.size
	local sizeScale = 0.15
	self.logo = spark:createQuad( vec2( 0.82, 0.92 ), -- position
		                          vec2( sizeScale*size, -size*aspect*sizeScale ),  --size
		                          self.logoMaterial, "HUDPass" )
end

function Module1BState:deactivate()
	print( "Module1BState:deactivate" )
	self.theNextState = ""
end

function Module1BState:nextState( currTime )
	theNextState = self.theNextState
end

theState = Module1BState:new()
theNextState = ""

