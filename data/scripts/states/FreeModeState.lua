----------------------------------------
-- Include standard libraries
local Button = require "button"
local Render = require "render"
local Sim = require "Sim"
local ESUModel = require "ESUModel"
----------------------------------------

--[[

--]]

FreeModeState = {}

function FreeModeState:new()
    print( "FreeModeState:new" )
    newObj = 
    { 
        buttons = {}, 
        hasVibrated = false,
        startTime = -1, 
        activationTime = 0,
        currTime = 0, 
        contactArea = 0,  -- area of contact (m^2) between tissue and electrode
		radiusOfContact = 0.0002,
		tool = 0,
        tissueDistance = 0,  -- distance to tissue in mm
        theNextState = "",
		temperature = 36.8,     --baichun 2014-03-21
		show_cuttask1_text = true,
    }
    self.__index = self
    return setmetatable(newObj, self)
end


function FreeModeState:load()
    print( "FreeModeState:load" )

    local isShadowOn = true

    Render:createDefaultRenderPasses( isShadowOn )

    Sim.load( self )

    ESUModel.theESUModel:createSpark()

	--self.targetWattage = 20  --Ganesh changed from 30
	--self.targetMode = ESUINPUT_CUT
	--Sim.createInstructionText( self, string.format( 
	--[[
	Change the MODE setting on the ESU 
	touchscreen to "CUT" mode (yellow),
  
	And set the power to %d watts.
 
	(Waiting...)
	]]--, --self.targetWattage ) )
		--self.step = "Settings"
		Sim.createInstructionText( self, 
[[Prototype Simulation State

Change the ESU and power 
settings to see the tissue
effects.
Press Enter to go back.
]])
		
end

function FreeModeState:activate()
    print( "FreeModeState:activate" )
    self.startTime = -1
    self.theNextState = ""

    Sim.activate( self )
end


function FreeModeState:update( dt )
    -- Convey updates from the UI to the current ESU settings
    ESUModel.theESUModel:updateInput( theESUInput )
	--self.targetWattage = ESUModel.theESUModel.cutWattage*0.1;
	self.instructionText:setText(string.format(
	[[
	Task 1: Set the electrosurgical pencil to
Pure coag on 30. Hold it in contact with the
tissue and activate it in a painting
like manner across the tissue.  

Task 2: Keep the electrosurgical pencil
on coag at 30. Hold it near but not in
direct contact with tissues and 
activate it in a painting like manner
across the tissue. 
 
Task 3: Set the electrosurgical pencil to
Pure coag on 50.Hold it near but not in direct
contact with tissues andactivate it in a painting
like manner across the tissue. 
  
Task 4: Set the electrosurgical pencil to Pure
coag at 100. Hold it near but not in direct 
contactwith tissues and activate it in a painting
like      manner across the tissue.  

	]]))
	--print (ESUModel.theESUModel.cutWattage*0.1)

		

	if input:isKeyDown( KEY_KP_SPACE ) then
	    --self.show_cuttask1_text = false
		--self.targetWattage = 40
		self.theNextState="FreeMode"
    end

	if input:isKeyDown( KEY_KP_ENTER ) then
       -- self.theNextState = "Menu"
	   --self.theNextState="Module1B"
	   self.theNextState="Module1"
    end

    Sim.update( self, dt )
end

function FreeModeState:deactivate()
    print( "FreeModeState:deactivate" )
end

function FreeModeState:nextState( currTime )
    -- if input:isKeyDown( KEY_ESC ) then
    --     self.theNextState = "Loading"
    -- end
 
    if input:isButtonPressed( "stylus", 2 ) then
        --self.theNextState = "Menu"  --baichun
    end
    theNextState = self.theNextState
end

theState = FreeModeState:new()
theNextState = ""

