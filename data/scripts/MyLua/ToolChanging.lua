﻿-------------------------------------------------------
local ToolChanging = {}
-------------------------------------------------------

function ToolChanging_Tool_1()
	print('tool_1')
	--Tool:Print()
	--print(Tool.tool_1)
	Tool.tool_1 = true
	Tool.tool_2 = false
	return true
end

function ToolChanging_Tool_2()
	print('tool_2')
	--Tool:Print()
	--print(Tool.tool_2)
	Tool.tool_1 = false
	Tool.tool_2 = true
	return true
end

function get_state ()
	if(Tool.tool_1) then
		print(1)
		return 1
	else
		print(2)
		return 2
	end	
end


-------------------------------------------------------
return ToolChanging
-------------------------------------------------------
