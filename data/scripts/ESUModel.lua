﻿local ESUModel = {}
--ESUModel.__index = ESUModel -- failed table lookups go to methods

function ESUModel:new()
	print( "ESUModel:new()" )
	newObj = 
	{
		voltage_global_coag = 0,
		voltage_global = 0,   -- Baichun 2014-03-21
		show_spark_distance = 0,  -- Baichun 2014-03-21
		last_mode = ESUINPUT_COAG, -- Baichun 2014-03-20
		b_widthOfInstrument = 0.005,
		cutWattage = 30,
		coagWattage = 30,
		mode = ESUINPUT_COAG,
		electrode = ESUINPUT_SPATULA,
		hasCreatedSpark = false,
		currentState = 1, -- added by Ganesh 2014-11-30
		ESUModeLabels = { [ESUINPUT_CUT] = "[Cut]", 
						  [ESUINPUT_COAG] = "[Coag]", 
						  [ESUINPUT_BLEND] = "[Blend]" }
	}
	self.__index = self
	return setmetatable( newObj, self )

	-- local self = setmetatable( self, ESUModel )
	-- self.cutWattage = 30
	-- self.coagWattage = 30
	-- self.mode = spark.ESUINPUT_BLEND
	-- self.electrode = spark.ESUINPUT_SPATULA

	-- self.sparkMat = spark:createMaterial( "texturedSparkShader" )
 --    self.sparkIntensity = 1.0
 --    self.sparkMat:setVec4( "u_color", vec4(1.0, 1.0, 0.5, self.sparkIntensity) )
 --    self.mySpark = spark:createLSpark( vec3(1.0, 0, -5), vec3(-1.0, 0, -5),
 --                                         1.0, --intensity
 --                                         0.25, --scale 
 --                                         3, --recursiveDepth
 --                                         0.5, --forkProb
 --                                         "TransparentPass",
 --                                         self.sparkMat )
 --    self.sparkActivationTime = 0
 --    self.sparkPeriod = 0.75

	-- return self
end

function ESUModel:createSpark( )
	self.sparkMat = spark:createMaterial( "texturedSparkShader" )
    self.sparkMat:setVec4( "u_color", vec4( 1.0, 1.0, 0.5, 1.0 ) )
    self.mySpark = spark:createLSpark( vec3(-0.05, 0, 0),
    	                          vec3( 0.05, 0, 0),
                                         1.0, --intensity
                                         0.25, --scale 
                                         3, --recursiveDepth
                                         0.5, --forkProb
                                         "TransparentPass",
                                         self.sparkMat )
    self.sparkActivationTime = 0
    self.sparkPeriod = 0.05
    self.hasCreatedSpark = true
end

function ESUModel:setCutWattage( wattage )
	self.cutWattage = wattage
end

function ESUModel:setCoagWattage( wattage )
	self.coagWattage = wattage
end

function ESUModel:setMode( mode )
	self.mode = mode
end

function ESUModel:setSimulationState(simState)
	self.currentState = simState
end
function ESUModel:update( dt )
	if self.hasCreatedSpark then
		self.mySpark:update( dt )
	end

	--/////////////////////////////////////////////////
	--baichun 2014-02-10
	--local widthOfInstrument = 0.005 --Baichun 2014-02-10
	if( input:isKeyDown(49) ) then   --GLFW_KEY_1  49
		self.b_widthOfInstrument = 0.00000--0.005	
		--print('test_test_1:')
		--print(self.b_widthOfInstrument)
	end
	if( input:isKeyDown(50) ) then   --GLFW_KEY_2  50
		self.b_widthOfInstrument = 0.001--0.006	
		--print('test_test_2:')
		--print(self.b_widthOfInstrument)
	end
	if( input:isKeyDown(51) ) then   --GLFW_KEY_3  51
		self.b_widthOfInstrument = 0.0025--0.007	
		--print('test_test_3:')
		--print(self.b_widthOfInstrument)
	end
	if( input:isKeyDown(52) ) then   --GLFW_KEY_4  52
		self.b_widthOfInstrument = 0.005--0.005
		--print('test_test_3:')
		--print(self.b_widthOfInstrument)
	end
	--/////////////////////////////////////////////////
end

function ESUModel:updateInput( esuInput )
	self.cutWattage = esuInput:cutWattage()*10
	self.coagWattage = esuInput:coagWattage()*10
	self.electrode = esuInput:electrode()--*10
	self.mode = esuInput:mode()

	self.voltage_global = self.cutWattage*0.1 -- Baichun   03-21-2014
	self.voltage_global_coag = self.coagWattage*0.1

	--Johnchy 2014-05-12
	if( input:isKeyDown(49) ) then   --GLFW_KEY_1  49
		self.cutWattage = self.cutWattage*20
		self.coagWattage = self.coagWattage*20
	end
	if( input:isKeyDown(50) ) then   --GLFW_KEY_2  50
		self.cutWattage = self.cutWattage*10
		self.coagWattage = self.coagWattage*10
	end
	if( input:isKeyDown(51) ) then   --GLFW_KEY_3  51
		self.cutWattage = self.cutWattage*5
		self.coagWattage = self.coagWattage*5
	end
	if( input:isKeyDown(52) ) then   --GLFW_KEY_4  52
		self.cutWattage = self.cutWattage*1
		self.coagWattage = self.coagWattage*1
	end
	--baichun 2014-03-10
	if( input:isKeyDown(53) ) then   --GLFW_KEY_5  53
		esuInput:keyboard_set_mode_cut()
	end
	if( input:isKeyDown(54) ) then   --GLFW_KEY_6  54
		esuInput:keyboard_set_mode_blend()
	end
	if( input:isKeyDown(55) ) then   --GLFW_KEY_7  55
		esuInput:keyboard_set_mode_coag()
	end
end

-- Precondition:  the mode and wattages (coag/cut) have already been set.
function ESUModel:activate( theTissueSim, xpos, ypos, stylusPos, tissueContactPos, distFromTissue, radius, dt )

	local touchThreshold = 0.0004 -- meters  

	local local_current = 0 --Baichun 2014-03-21
	local sparkThreshold = 0  --Baichun 2014-03-21

	if( self.mode == ESUINPUT_CUT ) then
		dutyCycle = 0.75 --0.65--0.5 --original 0.75  Baichun 2014-03-17  change the heat spreading speed to 0.3
		current = self.cutWattage * dutyCycle 
		voltage = self.cutWattage / dutyCycle
		touchThreshold = 0.002--*90/current -- meters  --Baichun 2014-03-21
		local_current = 5*current --Baichun 2014-03-21
		sparkThreshold = touchThreshold + local_current * 0.02/1000 --Baichun 2014-03-21
	end
	if( self.mode == ESUINPUT_COAG ) then
		dutyCycle = 0.75  --original 0.2  Baichun 2014-03-17
		current = self.coagWattage * dutyCycle
		voltage = self.coagWattage / dutyCycle
		touchThreshold = 0.002--*225/current -- meters  --Baichun 2014-03-21
		local_current = 2*current --Baichun 2014-03-21
		sparkThreshold = 0.008*current/225 + local_current * 0.02/1000  --Baichun 2014-03-21
	end
	if( self.mode == ESUINPUT_BLEND ) then
		--dutyCycle = 0.75  --original 0.5  Baichun 2014-03-17
		dutyCycle = 0.2 -- original was 0.5 Ganesh 2014-08-24
		current = (self.coagWattage + self.cutWattage ) * dutyCycle
		voltage = (self.coagWattage + self.cutWattage ) / dutyCycle
		touchThreshold = 0.002--*450/current -- meters  --Baichun 2014-03-21
		local_current = current --Baichun 2014-03-21
		sparkThreshold = 0.008*current/450 + local_current * 0.02/1000  --Baichun 2014-03-21
	end

	-- sparkThreshold is proportional to voltage
	-- 10 - 120
	-- min cut, v = 5, max cut = 60
	-- min coag, v = 100, max coag = 1200
	-- @1000V, spark threshold =ex 0.01
	-- @10V,  spark threshold = 0
	--local sparkThreshold = touchThreshold + local_current * 0.02/1000-- original Baichun 2014-03-21   voltage * 0.02/1000 -- meters/volts

	--print(sparkThreshold)
	--baichun 2014-03-13  change the spark length
	if(sparkThreshold > 0.025) then
		sparkThreshold = 0.025
	end

	--self.voltage_global = self.cutWattage*0.1 -- Baichun   03-21-2014
	--self.voltage_global_coag = self.coagWattage*0.1
	self.show_spark_distance = sparkThreshold    -- Baichun   03-21-2014

	--/////////////////////////////////////////////////
	--baichun 2014-02-10
	widthOfInstrument = self.b_widthOfInstrument
	--print(widthOfInstrument)
	--/////////////////////////////////////////////////


	if abs( distFromTissue ) < touchThreshold then
		-- Contact heating
		-- contact area proportional to the depth of penetration
		
		--if( input:isKeyDown(49) ) then   --GLFW_KEY_1  49
			--print('test_test_1')
		--end
		--local widthOfInstrument = 0.005 -- TODO -- get from electrode type   --(baichun 2014-02-10)original

		-- Random jitter for electricity placement
		xpos = xpos + widthOfInstrument * math.random()
		ypos = ypos + widthOfInstrument * math.random()

		-- print(string.format("Touch energy: (%2.2f, %2.2f, %2.2f, %2.2f, %2.2f, %2.2f, %2.2f)",
		-- 	xpos, ypos, 
		-- 	voltage, current, 
		-- 	dutyCycle, 
		-- 	radius, 
		-- 	dt ) )
		theTissueSim:accumulateElectricalEnergy( xpos, ypos, 
			voltage, current, 
			dutyCycle, 
			radius, 
			dt )
	elseif abs(distFromTissue) < sparkThreshold then
		-- Non-Contact heating
		-- create visual spark from worldCoord  

		-- local spreadAngleRadians = 90.0 * math.pi / 180.0 -- 90 degrees in radians
		-- local spreadSlope = math.tan( spreadAngleRadians * 0.5 )
		-- xpos = xpos + distFromTissue * math.random() * spreadSlope
		-- ypos = ypos + distFromTissue * math.random() * spreadSlope
		
		--if( input:isKeyDown(49) ) then   --GLFW_KEY_1  49
			--print('test_test_2')
		--end
		--local widthOfInstrument = 0.005 -- TODO -- get from electrode type   --(baichun 2014-02-10)original 

		if( self.mode == ESUINPUT_COAG ) then
			-- Hacky -- when in coag, always have larger spread
			widthOfInstrument = widthOfInstrument * 1.5
		end
		-- Random jitter for electricity placement
		xpos = xpos + widthOfInstrument * math.random()
		ypos = ypos + widthOfInstrument * math.random()

		-- Draw the spark
		if self.hasCreatedSpark then
			-- self.sparkActivationTime = self.sparkActivationTime + dt
			-- if self.sparkActivationTime > self.sparkPeriod then
			--        self.sparkActivationTime = 0

			--Baichun 2014-03-21
			local spark_scale = 0.33
			if(self.mode == ESUINPUT_CUT) then
				spark_scale = 0.05 + 0.05*local_current/450
			else 
				if(self.mode == ESUINPUT_COAG) then
					spark_scale = 0.33 + 0.05*local_current/450
				else
					spark_scale = 0.33 + 0.025*local_current/450
				end
			end 


	        self.mySpark:reseat( stylusPos, --vec3(-0.05, 0, 0),
	        	                 tissueContactPos, --vec3( 0.05, 0, 0),
	                             1.0, --intensity
	                             spark_scale,--0.33, --scale 
	                             4, --3, --recursiveDepth
	                             0.4 --forkProb
	                             )
		    -- end

			-- print(string.format("Spark energy: (%2.4f, %2.4f, %2.4f) (%2.4f, %2.4f, %2.4f)",
			-- 	stylusPos.x, stylusPos.y, stylusPos.z,  
			-- 	tissueContactPos.x, tissueContactPos.y, tissueContactPos.z  
			-- ) )
		end


		-- print(string.format("Spark energy: (%2.2f, %2.2f, %2.2f, %2.2f, %2.2f, %2.2f, %2.2f)",
		-- 	xpos, ypos, 
		-- 	voltage, current, 
		-- 	dutyCycle, 
		-- 	radius, 
		-- 	dt ) )
		theTissueSim:accumulateElectricalEnergy( xpos, ypos, 
			voltage, current, 
			dutyCycle, 
			radius, 
			dt )
	end


end

-- Shared ESUModel 
ESUModel.theESUModel = ESUModel:new()

return ESUModel