#ifndef MESH_HPP
#define MESH_HPP

#include "Spark.hpp"

#include "Renderable.hpp"
#include "Projection.hpp"

#define GLEW_STATIC
#include <GL/glew.h>
#include <GLFW/glfw3.h>

#include <Eigen/Dense>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <glm/gtx/string_cast.hpp>

#include <memory>
#include <iostream>
#include <vector>

#include "config.hpp"  // Defines DATA_PATH
#include "VertexAttribute.hpp"

// Forward declaration of AssetImporter class for mesh loading
struct aiMesh;

namespace spark
{
    // Standard vertex type for mesh objects
    class MeshVertex
    {
    public:
        GLfloat m_position[4];
        GLfloat m_normal[4];
        GLfloat m_diffuseColor[4];
        GLfloat m_texCoord[3];

        /// Set some sane initial values.
        MeshVertex( void )
        {
            for( size_t i = 0; i < 4; ++i )
            {
                m_position[i] = 0.0;
                m_normal[i] = 0.0;
                m_diffuseColor[i] = 1.0;
            }
            m_normal[0] = 1.0;
            m_texCoord[0] = 0.5; m_texCoord[1] = 0.5; m_texCoord[2] = 0.5;
        }

        /// Defines the names of the "in" data channels for the vertex shader
        /// By convention, per-vertex attributes start with "v_"
        static void acquireVertexAttributes( std::vector<VertexAttributePtr>& outShaderAttributes )
        {
            VertexAttributePtr position(new FloatVertexAttribute("v_position",
                3,
                sizeof(MeshVertex),
                (void*)offsetof(MeshVertex, m_position) )
                );
            outShaderAttributes.push_back( position );
        
            VertexAttributePtr normal(new FloatVertexAttribute("v_normal",
                3,
                sizeof(MeshVertex),
                (void*)offsetof(MeshVertex, m_normal) )
                );
            outShaderAttributes.push_back( normal );

            VertexAttributePtr diffuse(new FloatVertexAttribute("v_color",
                4,
                sizeof(MeshVertex),
                (void*)offsetof(MeshVertex, m_diffuseColor) )
                );
            outShaderAttributes.push_back( diffuse );
        
            VertexAttributePtr texCoord3d(new FloatVertexAttribute("v_texCoord",
                3,
                sizeof(MeshVertex),
                (void*)offsetof(MeshVertex, m_texCoord) )
                );
            outShaderAttributes.push_back( texCoord3d );
        }
    };


    /// Wraps glBufferData to load from a std::vector
    /// E.g.:
    /// std::vector< MeshVertex > verts;
    /// glBufferDataFromVector( GL_ARRAY_BUFFER, verts, GL_STATIC_DRAW );
    template <typename VertexType>
    inline void glBufferDataFromVector( GLenum targetBufferObjectType, const std::vector<VertexType>& data, GLenum usagePattern )
    {
        glBufferData( targetBufferObjectType,
                     data.size() * sizeof(VertexType), // size in bytes of entire vertex vector
                     &(data.front()), // address of first elem
                     usagePattern );
    }

    /// Mesh supports rendering of a indexed set of triangles.
    /// \todo template-tize over MeshVertex
    class Mesh : public Renderable
    {
    public:
        Mesh( void );

        /// Copy constructor
        Mesh( const Mesh& other );

        virtual ~Mesh();

        /// Renderable
        virtual void render( const RenderCommand& rc ) const override;

        void clearGeometry( void );
        void resizeVertexArray( size_t newSize );
        void setVertex( size_t i, const Eigen::Vector3f& a, 
            const Eigen::Vector2f& textureCoords, 
            const Eigen::Vector4f& color, 
            const Eigen::Vector3f& norm );
        size_t addVertex( const Eigen::Vector3f& a, 
                          const Eigen::Vector2f& textureCoords, 
                          const Eigen::Vector4f& color, 
                          const Eigen::Vector3f& norm );
        size_t addVertex( const MeshVertex& v );
        void addTriangleByIndex( unsigned int a, unsigned int b, unsigned int c );

        /// Adds a quadrilateral to the Mesh.
        /// Must call bindDataToBuffers() before
        /// rendering this mesh.
        ///
        ///  c   d
        ///  *---*
        ///  |\  |
        ///  + \ +
        ///  |  \|
        ///  *---*
        ///  a   b
        ///
        /// Indexes of triangles (CCW): (abc) (cbd)
        void addQuad( const glm::vec3& a, const glm::vec2& aCoord,
                      const glm::vec3& b, const glm::vec2& bCoord, 
                      const glm::vec3& c, const glm::vec2& cCoord, 
                      const glm::vec3& d, const glm::vec2& dCoord, 
                      const glm::vec3& norm );

        
        /// Adds a quadrilateral to the Mesh.
        /// Must call bindDataToBuffers() before
        /// rendering this mesh.
        ///
        ///  c   d
        ///  *---*
        ///  |\  |
        ///  + \ +
        ///  |  \|
        ///  *---*
        ///  a   b
        ///
        /// Indexes of triangles (CCW): (abc) (cbd)
        void addQuad( const Eigen::Vector3f& a, const Eigen::Vector2f& aCoord,
                      const Eigen::Vector3f& b, const Eigen::Vector2f& bCoord, 
                      const Eigen::Vector3f& c, const Eigen::Vector2f& cCoord, 
                      const Eigen::Vector3f& d, const Eigen::Vector2f& dCoord, 
                      const Eigen::Vector3f& norm );
    
        /// Bind geometry data to buffers
        void bindDataToBuffers( void );
    
        /// Bind shader to vertex data
        virtual void attachShaderAttributes( GLuint aShaderProgramIndex );

        /// Construction methods
        void unitCube( void );
        void cube( float scale );

        /// Add triangles for a plane positioned at center, with a normal
        /// vector at 0,0,1, and scale units in x,y directions.
        /// The plane is subdivided into the given number of quads along each axis.
        void plane( const glm::vec3& center,
                    const glm::vec2& scale,
                    const glm::ivec2& subdivisions );
    
        /// Load the mesh and associated materials from the given filename
        bool createMeshFromFile( const std::string& filename,
                                 TextureManagerPtr tm,
                                 ShaderManagerPtr sm,
                                 const RenderPassName& renderPassName );

        static MeshPtr createMeshFromAiMesh( const aiMesh* meshNode, 
                                             float scale = 1.0 );

        static RenderablePtr createBox( TextureManagerPtr tm, 
                                        ShaderManagerPtr sm,
                                        const RenderPassName& renderPassName );
        
    protected:
        GLuint m_vertexArrayObjectId;
        GLuint m_vertexBufferId;
        GLuint m_elementBufferId;
        std::vector< VertexAttributePtr > m_attributes;
        std::vector< MeshVertex > m_vertexData;
        std::vector< GLuint > m_vertexIndicies;
        glm::mat4 m_modelTransform;
    };
    typedef spark::shared_ptr< Mesh > MeshPtr;


    /// Illustrative example of a mesh that dynamically updates its vertices
    /// during a call to update.
    /// DO NOT USE -- just proof of concept. (a real version should deal
    /// with threading issues, or ensure that update is called with 
    /// the GL context)
    class UpdateableMesh : public Mesh, public Updateable
    {
    public:
        UpdateableMesh( MeshPtr sourceMesh );
        virtual ~UpdateableMesh() {}

        /// Example update showing mapping updates.  Should only call
        /// on thread with OpenGL context (i.e., not to be async updated)
        void update( double dt ) override;
    };
    typedef spark::shared_ptr< UpdateableMesh > UpdateableMeshPtr;
}
#endif

